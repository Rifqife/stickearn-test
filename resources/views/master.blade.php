<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Stickearn Test Game</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/datatables.min.css">
    <link rel="stylesheet" href="/css/fontawesome.min.css">
	<link rel="stylesheet" href="/css/yuurikka.css">
</head>
<body>
@section('content')
@show
<script type="text/javascript" src="/js/jquery3.min.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/datatables.min.js"></script>
<script type="text/javascript" src="/js/fontawesome.min.js"></script>
<script type="text/javascript" src="/js/sweetalert2.min.js"></script>
@section('additional_js')
@show
</body>
</html>
