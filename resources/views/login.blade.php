@extends('master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col"></div>
        <div class="col-5">
            <div class="form-header">
                <!-- <h3>Word Scramble Game</h3> -->
            </div>
            <div class="form-container">
                <form class="login-form" method="post">
                    {{ csrf_field() }}
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text"><i class="fas fa-user"></i></div>
                        </div>
                        <input type="text" class="form-control" name="username" placeholder="Username">
                    </div>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text"><i class="fas fa-lock"></i></div>
                        </div>
                        <input type="password" class="form-control" name="password" placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn-primary btn-block btn-lg" name="button">Login</button>
                </form>
            </div>
            <div class="form-footer mt-3">
                <p class="or-text">Or</p>
                <button type="button" data-toggle="modal" data-target="#register-form-modal" class="btn btn-warning btn-block btn-lg btn-register">Register</button>
            </div>
        </div>
        <div class="col"></div>
    </div>
    @include('register')
</div>
@endsection
@section('additional_js')
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('.btn-register').click(function(e) {
            $('#register-form-modal').modal();
        });
    });
</script>
@endsection
