<div class="modal fade" id="register-form-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Register New Account</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form class="register-form" method="post">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <label for="username" class="col-3 col-form-label">Username</label>
                        <div class="col">
                            <input type="text" name="username" class="form-control" placeholder="Example: John Doe">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-3 col-form-label">Email</label>
                        <div class="col">
                            <input type="email" name="email" class="form-control" placeholder="Example: johndoe@gmail.com">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-3 col-form-label">Password</label>
                        <div class="col">
                            <input type="password" name="password" class="form-control" placeholder="********************">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Register</button>
            </div>
        </div>
    </div>
</div>
